package app.support;

import java.util.ArrayList;
import java.util.List;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import app.dto.UserDTO;
import app.model.User;

@Component
public class UserToUserDTO implements Converter<User, UserDTO> {

	@Override
	public UserDTO convert( User user) {
		
		UserDTO userDTO = new UserDTO();
		
		userDTO.setId(user.getId());
		userDTO.seteMail(user.geteMail());
		userDTO.setName(user.getName());
		userDTO.setLastName(user.getLastName());
		userDTO.setUserName(user.getUserName());
		
		return userDTO;
	}
	
	public List <UserDTO> convert(List<User> users) {
		List<UserDTO> userDTOS = new ArrayList<>();
		
		for(User u : users) {
			UserDTO dto = convert(u);
			userDTOS.add(dto);
		}
		return userDTOS;
	}
}
