package app.support;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import app.dto.BusCarrierDTO;
import app.model.BusCarrier;
import app.service.BusCarrierService;

@Component
public class BusCarrierDTOToBusCarrier implements Converter<BusCarrierDTO, BusCarrier>{
	
	@Autowired
	private BusCarrierService busCarrierService;
	
	@Override
	public BusCarrier convert(BusCarrierDTO source) {
		BusCarrier busCarrier;
		
		if(source.getId() != null) {
			busCarrier = new BusCarrier();
		} else {
			busCarrier = busCarrierService.findOne(source.getId());
		}
		
		if (busCarrier != null) {
			busCarrier.setId(source.getId());
			busCarrier.setName(source.getName());
			busCarrier.setAdress(source.getAdress());
			busCarrier.setPib(source.getPib());
		}
		return busCarrier;
	}
	

}
