package app.support;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import app.dto.BusLineDTO;
import app.model.BusLine;
import app.service.BusCarrierService;
import app.service.BusLineService;

@Component
public class BusLineDtoToBusLine implements Converter<BusLineDTO, BusLine>{
	
	@Autowired
	private BusLineService busLineService;
	
	@Autowired
	private BusCarrierService busCarrierService;
	
	@Override
	public BusLine convert(BusLineDTO source) {
		BusLine busLine;
		
		if(source.getId() == null) {
			busLine = new BusLine();
		} else {
			busLine = busLineService.findOne(source.getId());
		}
		
		if(busLine != null) {
			busLine.setNumSeating(source.getNumSeating());
			busLine.setTicketPrice(source.getTicketPrice());
			busLine.setDestination(source.getDestination());
			busLine.setDepartureTime(source.getDepartureTime());
			if (source.getCarrier() != null) {
				busLine.setCarrier(busCarrierService.findOne(source.getCarrier().getId()));
			}
		}
		return busLine;
	}

}
