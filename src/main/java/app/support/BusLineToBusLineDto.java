package app.support;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import app.dto.BusLineDTO;
import app.model.BusLine;

@Component
public class BusLineToBusLineDto implements Converter<BusLine, BusLineDTO> {
	
	@Autowired
	private BusCarrierToBusCarrierDTO toBusCarrierDTO;
	
	@Override
	public BusLineDTO convert(BusLine source) {
		BusLineDTO dto = new BusLineDTO();
		
		dto.setId(source.getId());
		dto.setNumSeating(source.getNumSeating());
		dto.setTicketPrice(source.getTicketPrice());
		dto.setDestination(source.getDestination());
		dto.setDepartureTime(source.getDepartureTime());
		dto.setCarrier(toBusCarrierDTO.convert(source.getCarrier()));
		
		return dto;
	}
	
	public List<BusLineDTO> convert (List<BusLine> busLines) {
		List<BusLineDTO> busLinesDTO = new ArrayList<>();
		
		for(BusLine l: busLines) {
			busLinesDTO.add(convert(l));
		}
		return busLinesDTO;
	}

}
