package app.support;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import app.dto.UserDTO;
import app.model.User;
import app.service.UserService;

@Component
public class UserDTOToUser implements Converter<UserDTO, User>	{
	
	@Autowired
	private UserService userService;
	
	public User convert(UserDTO userDTO) {
		User user = null;
		if(userDTO.getId() != null) {
			user = userService.findOne(userDTO.getId()).get();
		}
		
		if(user == null) {
			user = new User();
		}
		
		user.setUserName(userDTO.getUserName());
		user.seteMail(userDTO.geteMail());
		user.setName(userDTO.getName());
		user.setLastName(userDTO.getLastName());
		
		return user;
	}

}
