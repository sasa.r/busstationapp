package app.support;

import java.util.ArrayList;
import java.util.List;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import app.dto.BusCarrierDTO;
import app.model.BusCarrier;


@Component
public class BusCarrierToBusCarrierDTO implements Converter <BusCarrier, BusCarrierDTO> {
	
	@Override
	public BusCarrierDTO convert (BusCarrier source) {
		BusCarrierDTO dto = new BusCarrierDTO();
		
		dto.setId(source.getId());
		dto.setName(source.getName());
		dto.setAdress(source.getAdress());
		dto.setPib(source.getPib());
		
		return dto;
		
	}
		
	public List<BusCarrierDTO> convert(List<BusCarrier> busCarriers) {
		
		List<BusCarrierDTO> busCarriersDTO = new ArrayList<>();
		
		for(BusCarrier b : busCarriers) {
			busCarriersDTO.add(convert(b));
		}
	
		return busCarriersDTO;
	}

}
