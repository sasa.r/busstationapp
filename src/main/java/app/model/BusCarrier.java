package app.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity
public class BusCarrier {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Column(unique = true, nullable = false)
	private String name;
	
	@Column
	private String adress;
	
	@Column(unique = true, nullable = false)
	private String pib;
	
	@OneToMany(mappedBy = "carrier", cascade = CascadeType.ALL)
	private List<BusLine> lines = new ArrayList<>();

	public BusCarrier(Long id, String name, String adress, String pib, List<BusLine> lines) {
		super();
		this.id = id;
		this.name = name;
		this.adress = adress;
		this.pib = pib;
		this.lines = lines;
	}

	public BusCarrier() {
		super();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAdress() {
		return adress;
	}

	public void setAdress(String adress) {
		this.adress = adress;
	}

	public String getPib() {
		return pib;
	}

	public void setPib(String pib) {
		this.pib = pib;
	}

	public List<BusLine> getLines() {
		return lines;
	}

	public void setLines(List<BusLine> lines) {
		this.lines = lines;
	}
}
