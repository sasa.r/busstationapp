package app.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

@Entity
public class BusLine {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Column(nullable = false)
	private int numSeating;
	
	@Column(nullable = false)
	private Double ticketPrice;
	
	@Column(nullable = false)
	private String destination;
	
	@Column(nullable = false)
	private String departureTime;
	
	@ManyToOne
	private BusCarrier carrier;

	public BusLine() {
		super();
	}

	public BusLine(Long id, int numSeating, Double ticketPrice, String destination, String departureTime,
			BusCarrier carrier) {
		super();
		this.id = id;
		this.numSeating = numSeating;
		this.ticketPrice = ticketPrice;
		this.destination = destination;
		this.departureTime = departureTime;
		this.carrier = carrier;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public int getNumSeating() {
		return numSeating;
	}

	public void setNumSeating(int numSeating) {
		this.numSeating = numSeating;
	}

	public Double getTicketPrice() {
		return ticketPrice;
	}

	public void setTicketPrice(Double ticketPrice) {
		this.ticketPrice = ticketPrice;
	}

	public String getDestination() {
		return destination;
	}

	public void setDestination(String destination) {
		this.destination = destination;
	}

	public String getDepartureTime() {
		return departureTime;
	}

	public void setDepartureTime(String departureTime) {
		this.departureTime = departureTime;
	}

	public BusCarrier getCarrier() {
		return carrier;
	}

	public void setCarrier(BusCarrier carrier) {
		this.carrier = carrier;
	}
}
