package app.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import app.model.BusCarrier;
import app.repository.BusCarrierRepository;
import app.service.BusCarrierService;

@Service
public class JpaBusCarrierService implements BusCarrierService {

	@Autowired
	private BusCarrierRepository busCarrierRepository;
	
	@Override
	public List<BusCarrier> findAll() {
		return busCarrierRepository.findAll();
	}
	
	@Override
	public BusCarrier findOne(Long id) {
		return busCarrierRepository.findOneById(id);
	}
	
	@Override
	public BusCarrier save(BusCarrier busCarrier) {
		return busCarrierRepository.save(busCarrier);
	}
}
