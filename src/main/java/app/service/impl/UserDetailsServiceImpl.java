package app.service.impl;

import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import app.model.User;
import app.service.UserService;

@Service
@Primary
public class UserDetailsServiceImpl implements UserDetailsService{

	@Autowired
	private UserService userService;
	
	/*
	 * We want to present users through the UserDetails class,the way Spring boot
	 * represents the user. We load based on username users from our database, and
	 * with UsersDetails we map his data credentials and role through GrantedAuthorities.
	 */
	
	@Override
	@Transactional
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		User user = userService.findByUserName(username).orElse(null);
		
		if(user == null) {
			throw new UsernameNotFoundException(String.format("No user found with username '%s'.", username));
		} else {
			List<GrantedAuthority> grantedAuthorities = new ArrayList<>();
			
			//user may have more than a one role,so for every role we can define rules
			String role = "ROLE_" + user.getRole().toString();
			//String role = user.getRole().toString();
			grantedAuthorities.add(new SimpleGrantedAuthority(role));
			
			return new org.springframework.security.core.userdetails.User(user.getUserName().trim(),
					user.getPassword().trim(), grantedAuthorities);
		}
	}
}
