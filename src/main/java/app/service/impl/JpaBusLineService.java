package app.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import app.model.BusLine;
import app.repository.BusLineRepository;
import app.service.BusLineService;

@Service
public class JpaBusLineService implements BusLineService {
	
	@Autowired
	private BusLineRepository busLineRepository;

	@Override
	public BusLine findOne(Long id) {
		return busLineRepository.findOneById(id);
	}

	@Override
	public BusLine save(BusLine busLine) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public BusLine update(BusLine busLine) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public BusLine delete(Long id) {
		// TODO Auto-generated method stub
		return null;
	}
	
	

}
