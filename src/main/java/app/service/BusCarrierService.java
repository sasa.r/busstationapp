package app.service;

import java.util.List;

import app.model.BusCarrier;

public interface BusCarrierService {

	List<BusCarrier> findAll();
	
	BusCarrier findOne(Long id);
	
	BusCarrier save(BusCarrier carrier);
	
}
