package app.service;

import app.model.BusLine;

public interface BusLineService {

	BusLine findOne(Long id);
	
	BusLine save(BusLine busLine);
	
	BusLine update(BusLine busLine);
	
	BusLine delete(Long id);
	
}
