package app.service;

import java.util.List;
import java.util.Optional;

import app.model.User;

public interface UserService {

	Optional<User> findOne(Long id);
	
	List<User> findAll();
	
	Optional<User> findByUserName(String username);
	
}
