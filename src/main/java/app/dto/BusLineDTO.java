package app.dto;


public class BusLineDTO {
	
	private Long id;
	
	private int numSeating;
	
	private Double ticketPrice;
	
	private String destination;
	
	private String departureTime;
	
	private BusCarrierDTO carrier;

	public BusLineDTO() {
		super();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public int getNumSeating() {
		return numSeating;
	}

	public void setNumSeating(int numSeating) {
		this.numSeating = numSeating;
	}

	public Double getTicketPrice() {
		return ticketPrice;
	}

	public void setTicketPrice(Double ticketPrice) {
		this.ticketPrice = ticketPrice;
	}

	public String getDestination() {
		return destination;
	}

	public void setDestination(String destination) {
		this.destination = destination;
	}

	public String getDepartureTime() {
		return departureTime;
	}

	public void setDepartureTime(String departureTime) {
		this.departureTime = departureTime;
	}

	public BusCarrierDTO getCarrier() {
		return carrier;
	}

	public void setCarrier(BusCarrierDTO carrier) {
		this.carrier = carrier;
	}
	
}
