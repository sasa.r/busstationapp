package app.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import app.model.BusCarrier;

@Repository
public interface BusCarrierRepository extends JpaRepository<BusCarrier, Long>{

	List<BusCarrier> findAll();
	
	BusCarrier findOneById(Long id);
	
	
}
