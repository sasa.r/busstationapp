package app.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import app.model.BusLine;

@Repository
public interface BusLineRepository extends JpaRepository<BusLine, Long>{
	
	BusLine findOneById(Long id);
	
	

}
