package app.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import app.model.User;

@Repository
public interface UserReository extends JpaRepository<User, Long>{
	
	Optional<User> findFirsByUserName(String userName);
	
	Optional<User> findFirstByUserNameAndPassword(String userName, String password);
	
}
