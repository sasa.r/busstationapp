package app.controller;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import app.dto.BusLineDTO;
import app.model.BusLine;
import app.service.BusLineService;
import app.support.BusLineDtoToBusLine;
import app.support.BusLineToBusLineDto;

@RestController
@RequestMapping(value = "api/lines", produces = MediaType.APPLICATION_JSON_VALUE) 
public class BusLineController {
	
	@Autowired
	private BusLineService busLineService;
	
	@Autowired
	private BusLineDtoToBusLine toBusLine;
	
	@Autowired
	private BusLineToBusLineDto toBusLineDTO;
	
//	@GetMapping
//	public ResponseEntity<List<BusLineDTO>> getAll() {
//		
//		List<BusLine> lines = busLineService.findAll
//	}

}
