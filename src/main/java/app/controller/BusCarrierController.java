package app.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import app.dto.BusCarrierDTO;
import app.model.BusCarrier;
import app.service.BusCarrierService;
import app.support.BusCarrierDTOToBusCarrier;
import app.support.BusCarrierToBusCarrierDTO;

@RestController
@RequestMapping(value = "/api/carriers", produces = MediaType.APPLICATION_JSON_VALUE)
public class BusCarrierController {
	
	@Autowired
	private BusCarrierService busCarrierService;

	@Autowired
	private BusCarrierToBusCarrierDTO toBusCarrierDTO;
	
	@Autowired
	private BusCarrierDTOToBusCarrier toBusCarrier;
	
	@GetMapping
	public ResponseEntity<List<BusCarrierDTO>>getAll (){
		List<BusCarrier> carriers = busCarrierService.findAll();
	
		return new ResponseEntity<>(toBusCarrierDTO.convert(carriers), HttpStatus.OK);
	}
	
	
}
